//
//  SSStreamRankingViewController.swift
//  UOL Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import UIKit

class SSStreamRankingViewController: SSBaseViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
}
