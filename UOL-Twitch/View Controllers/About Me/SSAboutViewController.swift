//
//  SSAboutViewController.swift
//  UOL Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import UIKit
import MessageUI

class SSAboutViewController: SSBaseViewController, MFMailComposeViewControllerDelegate {
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: - Actions
	@IBAction func openLinkedIn(_ sender: Any) {
		let url = URL(string: "linkedin://profile/iurichiba")!
		if UIApplication.shared.canOpenURL(url) {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		} else {
			self.openURLString("http://www.linkedin.com/in/iurichiba/")
		}
	}
	
	@IBAction func sendEmail(_ sender: Any) {
		if MFMailComposeViewController.canSendMail() {
			let composeVC = MFMailComposeViewController()
			composeVC.mailComposeDelegate = self
			composeVC.setToRecipients(["iurichiba@gmail.com"])
			composeVC.setSubject("Twitchy - Feedback")
			self.present(composeVC, animated: true, completion: nil)
		} else {
			self.serviceUnavailable((sender as! UIButton).title(for: .normal))
		}
	}
	
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		controller.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func callPhone(_ sender: Any) {
		let url = URL(string: "tel://+5511954524615")
		if UIApplication.shared.canOpenURL(url!) {
			UIApplication.shared.open(url!, options: [:], completionHandler: nil)
		} else {
			self.serviceUnavailable((sender as! UIButton).title(for: .normal))
		}
	}
	
	func serviceUnavailable(_ title: String?) {
		let alert = UIAlertController.init(title: "Warning!".localized, message: "Warning_ServiceUnavailable".localized, preferredStyle: .alert)
		alert.addAction(UIAlertAction.init(title: "OK", style: .cancel, handler: nil))
		self.present(alert, animated: true, completion: nil)
	}
	
}
