//
//  SSBaseViewController.m
//  UOL Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

// Interface
#import "SSBaseViewController.h"

// Libraries
#import <MBProgressHUD/MBProgressHUD.h>

// Helpers
#import "NSString+SSHelpers.h"

// Constants
#define kHUD_DefaultTitle @"HUD_DefaultTitle".localized

@interface SSBaseViewController()

@end

@implementation SSBaseViewController

#pragma mark - Initialization
- (void)viewDidLoad {
	[super viewDidLoad];
	// TODO: Implement Analytics
}

#pragma mark - MBProgressHUD Helpers
// Custom description
- (void(^)(void))showHudWithDescription:(NSString *)description {
	return [self showHudWithTitle:kHUD_DefaultTitle andDescription:description addedTo:self.view];
}

// Title + description
- (void(^)(void))showHudWithTitle:(NSString *)title andDescription:(NSString *)description {
	return [self showHudWithTitle:title andDescription:description addedTo:self.view];
}

// Description + view
- (void(^)(void))showHudWithDescription:(NSString *)description addedTo:(UIView *)view {
	return [self showHudWithTitle:kHUD_DefaultTitle andDescription:description addedTo:view];
}

// All customizations
- (void(^)(void))showHudWithTitle:(NSString *)title andDescription:(NSString *)description addedTo:(UIView *)view {
	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
	hud.label.text = title;
	hud.detailsLabel.text = description;
	return ^{[hud hideAnimated:YES];};
}

#pragma mark - Integration Tests
- (void)logTest {
	NSLog(@"Testing, testing! Beep beep!");
}

#pragma mark - Connectivity Helpers
- (void)openURLString:(NSString *)urlString {
	NSURL *url = [NSURL URLWithString:urlString];
	if (@available(iOS 10, *))
		[UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];
		else [UIApplication.sharedApplication openURL:url];
}

@end
