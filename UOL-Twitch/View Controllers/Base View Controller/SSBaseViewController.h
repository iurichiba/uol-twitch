//
//  SSBaseViewController.h
//  UOL Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSBaseViewController: UIViewController

#pragma mark - MBProgressHUB Helpers
// Custom description
- (void(^)(void))showHudWithTitle:(NSString *)title andDescription:(NSString *)description;
// Title + description
- (void(^)(void))showHudWithDescription:(NSString *)description;
// Description + view
- (void(^)(void))showHudWithTitle:(NSString *)title andDescription:(NSString *)description addedTo:(UIView *)view;
// All customizations
- (void(^)(void))showHudWithDescription:(NSString *)description addedTo:(UIView *)view;

#pragma mark - Integration Tests
- (void)logTest;

#pragma mark - Connectivity Helpers
- (void)openURLString:(NSString *)urlString;

@end
