//
//  SSGameRankingViewController.swift
//  UOL Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import Foundation
import UIKit
import Hero
import Moya
import Sugar
import AlamofireImage

class SSGameRankingViewController: SSBaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITabBarControllerDelegate {

	// MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
	
	// MARK: Variables
	var games: [SSGameModel] = []
	var maxResults: Int = 10
	
	// MARK: Constants
	
	// MARK: - Initialization
    override func viewDidLoad() {
        super.viewDidLoad()
		self.loadGamesWithOffset(0)
		self.heroAnimationSetup()
		self.rewindShortcutSetup()
    }
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		NotificationCenter.default.post(name: .revertCellAnimation, object: nil)
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	func heroAnimationSetup() {
		self.navigationController?.hero.navigationAnimationType = .selectBy(presenting: .fade, dismissing: .fade)
	}
	
	// MARK: - Content Manipulation
	var loading = false
	
	func loadGamesWithOffset(_ offset: Int) {
		// No concurrent requests
		guard !loading else {
			return
		}
		
		// Can't request if there aren't any more games :shrug:
		guard maxResults > games.count else {
			return
		}
		
		// Get games w/ set offset
		loading = true
		SSGameModel.loadGameRankingWithOffset(offset) { (success, error, maxResults, ranking) in
			if maxResults > 0 { self.maxResults = maxResults }
			self.collectionView.performBatchUpdates({
				for newGame in ranking ?? [] {
					self.games.append(newGame)
					self.collectionView.insertItems(at: [IndexPath(item: self.games.count, section: 0)])
				};
			}, completion: { (completion) in
				self.loading = false
			})
		}
	}
	
	func loadNextPage() {
		loadGamesWithOffset(self.games.count)
	}
	
	// MARK: - Rewind Shortcut
	func rewindShortcutSetup() {
		self.tabBarController?.delegate = self
	}
	
	func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
		if let vc = (viewController as? UINavigationController)?.viewControllers.first {
			if vc is SSGameRankingViewController {
				(vc as! SSGameRankingViewController).collectionView.setContentOffset(.zero, animated: true)
			}
		}
	}
	
	// MARK: - Collection View Delegate & Data Source
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return collectionView.frame.size
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return games.count + 1 // extra cell for the Welcome View
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let width = scrollView.contentSize.width
		let offset = scrollView.contentOffset.x
		// If five items to the end...
		if (width-offset <= scrollView.frame.size.width*6) {
			// Request next page
			loadNextPage()
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		// No animation for the Welcome View
		guard indexPath.item > 0 else {
			return
		}
		
		// Game Cell display animation
		UIView.animate(withDuration: 0.75, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.75, options: .curveEaseOut, animations: {
			cell.transform = .identity
			cell.alpha = 1.0
		}, completion: nil)
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		// Welcome View
		guard indexPath.item > 0 else {
			return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SSGameRankingWelcomeCell.self), for: indexPath)
		}
		
		// Game Cell
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SSGameRankingItemCell.self), for: indexPath) as! SSGameRankingItemCell
		cell.setupWithGame(games[indexPath.item-1], indexPath)
		// Animation reset
		cell.transform = .init(scaleX: 0.5, y: 0.5)
		cell.alpha = 0.0
		return cell
	}
	
	// MARK: - Open Game Details
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		// No action for the Welcome View
		guard indexPath.item > 0 else {
			return
		}
		
		// Cell must be of SSGameRankingItemCell type
		let cell = collectionView.cellForItem(at: indexPath) as? SSGameRankingItemCell
		guard cell != nil else {
			return
		}
		
		// Animate selection
		cell!.animateSelection() {
			cell!.alpha = 0.0
			NotificationCenter.default.addObserver(cell!, selector: #selector(cell?.resetAnimationState), name: .revertCellAnimation, object: nil)
			self.performSegue(withIdentifier: String(describing: SSGameDetailViewController.self), sender: self.games[indexPath.item-1])
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if (segue.identifier == String(describing: SSGameDetailViewController.self)) {
			let destination = segue.destination as! SSGameDetailViewController
			destination.game = sender as? SSGameModel
		}
	}
	
}

class SSGameRankingWelcomeCell: UICollectionViewCell {
	
}

class SSGameRankingItemCell: UICollectionViewCell {
	
    @IBOutlet weak var gameCover: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var gameViewers: UILabel!
    @IBOutlet weak var gameChannels: UILabel!
    
    @IBOutlet weak var informationContainer: UIView!
    
	func setupWithGame(_ game: SSGameModel, _ indexPath: IndexPath) {
        // Set Image
		gameCover.image = #imageLiteral(resourceName: "Placeholder")
		if let URL = URL(string: game.imageURL ?? "😐") {
			gameCover.af_setImage(withURL: URL)
			gameCover.hero.id = game.imageURL
		}
        
        // Set Info
		self.gameName.text = "#\(indexPath.item): \(game.name!)"
        self.gameViewers.text = "\(game.viewerCount!) \("GameItem_Viewers".localized)"
        self.gameChannels.text = "\(game.channelsCount!) \("GameItem_Streamers".localized)"
	}
	
	// MARK: - Segue Animation
	func animateSelection(_ then: (()->())? = nil) {
		UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseOut, animations: {
			self.transform = .init(scaleX: 0.75, y: 0.75)
			self.informationContainer.transform = .init(translationX: 0.0, y: self.informationContainer.frame.size.height)
			self.informationContainer.alpha = 0.0
		}) { (completion) in
			then?()
		}
	}
	
	@objc func resetAnimationState() {
		self.alpha = 1.0
		UIView.animate(withDuration: 0.25) {
			self.transform = .identity
			self.informationContainer.transform = .identity
			self.informationContainer.alpha = 1.0
		}
	}
    
}

extension NSNotification.Name {
	
	static let revertCellAnimation = NSNotification.Name(rawValue: "revertCellAnimation")
	
}
