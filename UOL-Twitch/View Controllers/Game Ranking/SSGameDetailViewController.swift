//
//  SSGameDetailViewController.swift
//  Production
//
//  Created by Iuri Chiba on 4/23/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import UIKit
import Hero
import AlamofireImage

class SSGameDetailViewController: SSBaseViewController {
	
	// MARK: Outlets
	@IBOutlet weak var gameCover: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var viewerCount: UILabel!
    @IBOutlet weak var channelCount: UILabel!
    
    @IBOutlet weak var shareButton: UIButton!
    
    // MARK: Variables
	var game: SSGameModel? = nil
	
	// MARK: - Initialization
	override func viewDidLoad() {
		super.viewDidLoad()
		setGameInformation()
		shareButtonSetup()
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		showShareButton()
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	// MARK: - Game Information
	func setGameInformation() {
        // No game, wtf
        guard game != nil else {
            return
        }
        
        // Set Image
		gameCover.image = #imageLiteral(resourceName: "Placeholder")
		if let URL = URL(string: game!.imageURL ?? "😐") {
			gameCover.af_setImage(withURL: URL)
			gameCover.hero.id = game!.imageURL
		}
        
        // Set Info
        self.gameName.text = game!.name!
        self.viewerCount.text = "\(game!.viewerCount!) \("GameItem_Viewers".localized)"
        self.channelCount.text = "\(game!.channelsCount!) \("GameItem_Streamers".localized)"
	}
	
	// MARK: - Actions
	@IBAction func backButtonTapped(_ sender: Any) {
		self.hero.dismissViewController()
	}
	
	@IBAction func googleButton(_ sender: Any) {
		openNameSearchWithBaseURL("http://lmgtfy.com/?q=")
	}
	
	@IBAction func steamButton(_ sender: Any) {
		openNameSearchWithBaseURL("http://store.steampowered.com/search/?term=")
	}
	
	func openNameSearchWithBaseURL(_ baseURL: String) {
		var stringURL = "www.google.com"
		if let encodedName = game?.name?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
			stringURL = baseURL + encodedName
		}; self.openURLString(stringURL)
	}
	
	// MARK: Share Button
	func shareButtonSetup() {
		var transform = self.shareButton.transform
		transform = transform.translatedBy(x: 0.0, y: 54.0)
		transform = transform.scaledBy(x: 0.01, y: 0.01)
		self.shareButton.transform = transform
	}
	
	func showShareButton() {
		UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
			self.shareButton.transform = .identity
		}, completion: nil)
	}
	
	@IBAction func shareButtonTapped(_ sender: Any) {
		let text = "\("Let's watch".localized) \(game!.name!) \("on Twitch!".localized)"
		let image = gameCover.image!
		let activityViewController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
		self.present(activityViewController, animated: true, completion: nil)
	}
	
}
