//
//  SSGameModel.swift
//  UOL-Twitch
//
//  Created by Iuri Chiba on 4/22/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import ObjectMapper

class SSGameModel: Mappable {
	
	var name: String?
	var imageURL: String?
	var viewerCount: Int?
	var channelsCount: Int?
	
	required init?(map: Map) {
		// TODO: Implement default initialization w/ default values
		// Still unused, won't affect the app
	}
	
	func mapping(map: Map) {
		name			<- map["game.name"]
		imageURL		<- map["game.box.large"]
		viewerCount		<- map["viewers"]
		channelsCount	<- map["channels"]
	}
	
}

extension SSGameModel {
	
	static func loadGameRankingWithOffset(_ offset: Int = 0, response: @escaping (_ success: Bool, _ error: Error?, _ maxResults: Int, _ ranking: [SSGameModel]?) -> ()) {
		let service = SSTwitchService.getInstance()
		service.request(.getGameRanking(offset: offset)) { result in
			switch result {
			case let .success(moyaResponse):
				do {
					try _ = moyaResponse.filterSuccessfulStatusCodes()
					let data = try moyaResponse.mapJSON()
					let maxResults = ((data as? NSDictionary) ?? [:])["_total"] as? Int ?? -1
					response(true, nil, maxResults, self.buildMultipleFromJSON(JSON: data as? [String : Any] ?? [:]))
				} catch {
					response(false, NSError.init(domain: moyaResponse.request?.url?.absoluteString ?? "Invalid domain", code: moyaResponse.statusCode, userInfo: nil), -1, nil)
				}
			case let .failure(error):
				response(false, error, -1, nil)
			}
		}
	}
	
	private static func buildMultipleFromJSON(JSON: [String: Any]) -> [SSGameModel]? {
		return Mapper<SSGameModel>().mapArray(JSONObject: JSON["top"] as? NSArray)
	}
	
}
