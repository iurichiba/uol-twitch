//
//  SSInfoHelpers.swift
//  UOL-Twitch
//
//  Created by Iuri Chiba on 4/22/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

class SSInfoHelpers: NSObject {
	
	enum AppKeys: String {
		case TwitchClientId = "twitch_client_id"
	}
	
	static let appKeysKey = "App Keys"
	static func getAppKeyForKeyPath(_ keyPath: AppKeys) -> String {
		let appKeys = Bundle.main.object(forInfoDictionaryKey: appKeysKey) as? NSDictionary
		return appKeys?.value(forKeyPath: keyPath.rawValue) as? String ?? "Invalid Data"
		
	}
	
}
