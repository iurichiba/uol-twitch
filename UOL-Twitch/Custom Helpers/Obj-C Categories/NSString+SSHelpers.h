//
//  NSString+SSHelpers.h
//  UOL-Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SSHelpers)

- (NSString *)localized;

@end
