//
//  NSString+SSHelpers.m
//  UOL-Twitch
//
//  Created by Iuri Chiba on 4/21/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

#import "NSString+SSHelpers.h"

@implementation NSString (SSHelpers)

- (NSString *)localized {
	return NSLocalizedString(self, nil);
}

@end
