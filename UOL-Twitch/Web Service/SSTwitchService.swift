//
//  SSTwitchService.swift
//  UOL-Twitch
//
//  Created by Iuri Chiba on 4/22/18.
//  Copyright © 2018 Shoryuken Solutions. All rights reserved.
//

import Moya

// MARK: - List of Configured Services
enum SSTwitchService {
	case getGameRanking(offset: Int)
}

// MARK: - TargetType Protocol Implementation
extension SSTwitchService: TargetType {
	
	static let sharedInstance = SSTwitchService.getInstance()
	
	static func getInstance() -> MoyaProvider<SSTwitchService> {
		#if DEBUG
		return MoyaProvider<SSTwitchService>(stubClosure:MoyaProvider.immediatelyStub)
		#else
		return MoyaProvider<SSTwitchService>()
		#endif
	}
	
	var baseURL: URL { return URL(string: "https://api.twitch.tv/")! }
	var path: String {
		switch self {
		case .getGameRanking:
			return "kraken/games/top"
		}
	}
	var method: Moya.Method {
		switch self {
		case .getGameRanking:
			return .get
		}
	}
	var task: Task {
		switch self {
		case .getGameRanking(let offset):
			return .requestParameters(parameters: ["offset": offset], encoding: URLEncoding.queryString)
		}
	}
	var sampleData: Data {
		// payload for DEBUG schemes (e.g., Development)
		switch self {
		case .getGameRanking(let offset):
			// Mock first page
			var url: URL? = nil
			if offset <= 0 {
				url = Bundle.main.url(forResource: "mock-topgames-page1", withExtension: "json")
			} else {
				url = Bundle.main.url(forResource: "mock-topgames-page2", withExtension: "json")
			}
			
			guard (url != nil) else {
				return Data()
			}; return (try? Data(contentsOf: url!)) ?? Data()
		}
	}
	var headers: [String: String]? {
		return ["Client-ID": SSInfoHelpers.getAppKeyForKeyPath(.TwitchClientId)]
	}
	
}
