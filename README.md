![Header](docs/header.png)

## Desafio iOS: Twitch

Este projeto é a minha resolução do desafio iOS para a UOL. Segue [enunciado](docs/README.md) para melhor entendimento.

### Twitchy
O nome Twitchy foi escolhido com o propósito de trazer alguma identidade para o aplicativo, ao invés de manter como "Desafio iOS"

## Requisitos
Para rodar o aplicativo será necessário:  

- Xcode 9.0+
- CocoaPods

## Disclaimer
Alguns ícones foram retirados de graça do website [FlatIcon](www.flaticon.com). Identidade, ícone do aplicativo e telas foram feitas por mim.

## Estrutura

O aplicativo está dividido em 2 targets e schemes (que estão como _shared_ no projeto para facilitar o uso): Development & Production.

- **Development:** para testes, carrega dados _mockados_ em arquivos .json dentro do projeto;
- **Production:** para "produção", carrega os dados direto da API do Twitch.

Além disso, o aplicativo foi localizado em 2 idiomas: Português e inglês. Para testes, recomendo trocar o parâmetro de idioma nos schemes do projeto.

## Funcionalidades

### Ranking de Jogos
O ranking de jogos carrega, com páginas de 10 em 10 (valor default da API), os jogos mais populares no Twitch no momento.

**OBS:** faltando ~5 itens na navegação, uma nova requisição é feita para carregar a próxima página, criando um efeito de lista infinita.

### Detalhes do Jogo
Infelizmente, não temos uma descrição do jogo, nem mesmo as plataformas que ele está disponível. Então, para diferenciar um pouco da visão da lista, existem algumas outras funcionalidades na tela de detalhes:

- **Mais detalhes:** uma busca pelo nome do jogo, para procurar mais detalhes;
- **Comprar na Steam:** um link para a Steam, para que o usuário possa comprar o jogo (deep-link não foi feito);
- **Compartilhar c/ Amigos:** compartilhamento de uma mensagem utilizando o UIActivityViewController para que o usuário possa escolher o melhor meio para compartilhar (e porque o Social Framework foi depreciado).

## Detalhes da Implementação

- O Framework SSUIComponents contém extensões para algumas classes como UIView e UIButton, facilitando a implementação de detalhes visuais com @IBDesignable e @IBInspectable

- A classe SSBaseViewController contém código em Objective-C, por conta de especificação no enunciado  
_(e porque gosto de Objective-C, haha)_

- Uma das primeiras implementações que fiz foram algumas funções auxiliares para o uso do MBProgressHUD, mas não houve necessidade de utilizar

## Bibliotecas utilizadas

- **Moya:** encapsulador do Alamofire, grande ajuda na organização do código de WebService. Em retrospecto, talvez tenha sido _overkill_ para um projeto desses;
- **AlamofireImage:** ótima biblioteca para facilitar o carregamento e cacheamento de imagens;
- **Sugar:** conjunto de extensões, ajudam bastante com a produtividade;
- **ObjectMapper:** biblioteca para mapeamento de objetos JSON em models _Mappable_, diminuindo drasticamente a quantidade de código necessário;
- **Hero:** framework para transições.

Algumas outras bibliotecas estão inclusas no Podfile, algumas até instaladas, mas acabei não utilizando por tempo ou falta de necessidade.

## Contato
Em caso de dúvida, feedback, etc, entrem em contato comigo por:

- **LinkedIn:** www.linkedin.com/in/iurichiba
- **E-mail:** iurichiba@gmail.com
- **Telefone:** +55 11 95452-4615